"-------------------------------------------------------------------------------------------------- 
" Vundle supported plugin's.
"--------------------------------------------------------------------------------------------------

" Get Vundle up and running.
set nocompatible
filetype off 
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree.git'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'tpope/vim-fugitive'
Plugin 'christoomey/vim-tmux-navigator'
Bundle 'jistr/vim-nerdtree-tabs'
Plugin 'mileszs/ack.vim'
Plugin 'morhetz/gruvbox'
Plugin 'djoshea/vim-autoread'
Plugin 'majutsushi/tagbar'
call vundle#end() 
filetype plugin indent on

"--------------------------------------------------------------------------------------------------
" Settings 
"--------------------------------------------------------------------------------------------------

" Disable compatibility mode
set nocp

" Turn on syntax highlighting.
syntax on

" Enable line numbers.
set number

" Backup files.
set nobackup

" No write backup.
set nowritebackup

" No swap file.
set noswapfile

" Show current row and column.
set ruler

" Always show the status bar.
set laststatus=2

" Convert tabs to spaces.
set expandtab

" Set tab size in spaces (this is for manual indenting).
set tabstop=4

" The number of spaces inserted for a tab (used for auto indenting).
set shiftwidth=4

" Allow 256 colors.
set t_Co=256

" Disable background color erase. Required when running TERM=xterm-256color
" inside tmux.
set t_ut=

" 100 column indicator.
set colorcolumn=121
highlight ColorColumn ctermbg=lightgray

" Highlight search matches.
"set hlsearch

" Ignore case in search unless an upper-case letter is used. 
set smartcase
set ignorecase

" Highlight the current line.
" set cursorline

" Highlight a matching [{()}] when cursor is placed on start/end character.
" set showmatch

" Better splits (new windows appear below and to the right).
set splitbelow 
set splitright

" Turn word wrap off.
set nowrap

" Enable mouse use in all modes.
set mouse=a

" Visual autocomplete for command menu.
"set wildmenu
"set wildmode=list:longest,full

" Specify leader for custom commands.
:let mapleader = "\<SPACE>"

" Prevent matchparen.vim from loading to disable {[()]} highlighting.
:let loaded_matchparen = 1

" Set color scheme. See http://vim.wikia.com/wiki/Switch_color_schemes for setting color schemes
" using script.
colorscheme zenburn
"colorscheme railscasts
"colorscheme gruvbox

" This is what sets vim to use 24-bit colors. It will also work for any version of neovim
" newer than 0.1.4.
"set termguicolors

" To allow backspacing over everything in insert mode (including automatically inserted indentation, 
" line breaks and start of insert).
:set backspace=indent,eol,start

" Configure initial size of gvim.
if has("gui_running")
  set lines=45 columns=150
endif

" Set the number of context lines you would like to see above and below the cursor when scrolling.
:set scrolloff=5

" Configure the statusline.
function! DerekFugitiveStatusLine()
  let status = fugitive#statusline()
  let trimmed = substitute(status, '\[Git(\(.*\))\]', '\1', '')
  let trimmed = substitute(trimmed, '\(\w\)\w\+\ze/', '\1', '')
  let trimmed = substitute(trimmed, '/[^_]*\zs_.*', '', '')
  if len(trimmed) == 0
    return ""
  else
    return '(' . trimmed[0:10] . ')'
  endif
endfunction
set stl=%f\ %m\ %r%{DerekFugitiveStatusLine()}\ Line:%l/%L[%p%%]\ Col:%v\ Buf:#%n\ [%b][0x%B]

" Toggle highlighting the current screen line based on vim mode.
:autocmd InsertEnter * set cul
:autocmd InsertLeave * set nocul

"--------------------------------------------------------------------------------------------------
" Configure YouCompleteMe
"--------------------------------------------------------------------------------------------------
" Move up and down in autocomplete with <c-j> and <c-k>
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

"--------------------------------------------------------------------------------------------------
" Configure Tagbar
"--------------------------------------------------------------------------------------------------

nmap <F6> :TagbarToggle<CR>

"--------------------------------------------------------------------------------------------------
" Configure CtrlP
"--------------------------------------------------------------------------------------------------

" CtrlP - configure.
set runtimepath^=~/.vim/bundle/ctrlp.vim

" CtrlP - show hidden files.
let g:ctrlp_show_hidden=1

" CtrlP - order matching files from top to bottom.
let g:ctrlp_match_window='bottom,order:ttb'

" CtrlP - always open files in new buffers.
" let g:ctrlp_switch_buffer=0

" CtrlP - change the working directory during a vim session and make CtrlP respect that change.
let g:ctrlp_working_path_mode=0

" CtrlP - file ignore list.
set wildignore+=*/.git/*,*/.metdata/*,.cproject,*/.inc/*,*/.d/*,*/.obj/*,*/bin/*,.project,*/.settings/*

" Shortcut to CtrlP buffer selection.
nnoremap <leader>b :CtrlPBuffer<CR>

"--------------------------------------------------------------------------------------------------
" Configure NERD tree
"--------------------------------------------------------------------------------------------------

" Allow a mouse to be used with NERD tree.
let g:NERDTreeMouseMode=3 


" Open NERDTree by default.
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window if the only buffer that's left is the NERDTree buffer.
function! s:CloseIfOnlyNerdTreeLeft()
    if exists("t:NERDTreeBufName")
        if bufwinnr(t:NERDTreeBufName) != -1
            if winnr("$") == 1
                q
            endif
        endif
    endif
endfunction

" Open NERDTree on vim startup.
let g:nerdtree_tabs_open_on_gui_startup=1
let g:nerdtree_tabs_open_on_console_startup=1

nmap <F5> :NERDTreeToggle<CR>

"-----------------------------------------------------------------------------
" Fugitive
"-----------------------------------------------------------------------------

" Thanks to Drew Neil
autocmd User fugitive
  \ if fugitive#buffer().type() =~# '^\%(tree\|blob\)$' |
  \  noremap <buffer> .. :edit %:h<cr> |
  \ endif
autocmd BufReadPost fugitive://* set bufhidden=delete

"-----------------------------------------------------------------------------
" Ack/Ag
"-----------------------------------------------------------------------------

" Use Ag with ack.vim.
let g:ackprg = 'ag --nogroup --nocolor --column'

"--------------------------------------------------------------------------------------------------
" Key Mapping 
"--------------------------------------------------------------------------------------------------

" Disable arrow keys for navigation.
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Remap escape key.
inoremap kj <ESC>

" Bind K to grep word under cursor.
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" Substitute all occurrences of the word under the cursor. Given this mapping, if the cursor is on 
" the word foo and you press \s (assuming the default backslash Leader key), you will see:
" :%s/\<foo\>/
" If you now type bar/g and press Enter, you will change all foo to bar. The \< and \> ensure that 
" only complete words are found (the search finds foo but not food).
:nnoremap <leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>

" Workaround for clang-complete that seems to work. Ctrl-] was only taking to the declaration rather 
" than defintion. Gave remapping a try and it worked.
:nnoremap <leader>] :tag <C-r><C-w><CR>

" Toggle NERD Tree.
map <F2> :NERDTreeTabsToggle<CR>

" Switch between files in buffer
nnoremap <C-Tab> :bn<CR>
nnoremap <C-S-Tab> :bp<CR>

" YouCompleteMe
nnoremap <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>

" manually, regenerate ctags
map <Leader>rt :!ctags -R .<CR><CR>

" Easier split navigation: instead of ctrl-w then j, it’s just ctrl-j.
"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>
nnoremap <leader>h <C-W><C-S>
nnoremap <leader>v <C-W><C-V>
nnoremap <C-W> <C-W><C-W>
nnoremap <C-O> <C-W><C-O>
nnoremap <C-C> <C-W><C-C>

" Open the current file into its own tab (i.e. workaround to implement "maximize current split".
nnoremap <C-UP> :tabe%<CR>
nnoremap <C-S-UP> :tabclose<CR>
